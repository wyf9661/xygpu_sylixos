#include <stdio.h>
#include <sys/ioctl.h>

struct st {
    int a;
    int b;
};

int main (int argc, char **argv)
{
    printf("Hello SylixOS!\n");
    printf("unsigned long size:%lu\n", sizeof(unsigned long));
    int fd = open("/dev/xygpu", 0644);

    if (fd <= 0)
    {
        printf("open the file failed...\n");
        exit(1);
    }

    int val = 2000;
    printf("=================old var %d , %x\n", val, &val);
    int ret = ioctl(fd, 200, &val);

    printf("=================ret = %d new val=%d \n", ret, val);

    struct st x_st = {
            .a = 201,
            .b = 202
    };

    printf("=================st %d ,a=%d, b=%d\n", &x_st, x_st.a, x_st.b);
    ret = ioctl(fd, 201, &x_st);

    printf("=================ret = %d \n", ret);
    printf("=================new a=%d, b=%d\n", x_st.a, x_st.b);


    close(fd);


    return  (0);
}
