/*
 * xygpu_osk_misc.c
 *
 *  Created on: Oct 18, 2022
 *      Author: paky
 */

/**
 * @file xygpu_osk_misc.c
 * Implementation of the OS abstraction layer for the kernel device driver
 */
#define  __SYLIXOS_KERNEL
#define  __SYLIXOS_PCI_DRV
#include <SylixOS.h>
#include <linux/kernel.h>
#include <stdarg.h>
#include <stdio.h>
//#include "xygpu_osk.h"

//just use printk to replace the "_xygpu_osk_dbgmsg"
//void _xygpu_osk_dbgmsg( const char *fmt, ... )
//{
//    static   CHAR          cBspMsgBuf[1024];
//    va_list args;
//    va_start(args, fmt);
//    vsnprintf(cBspMsgBuf, 1023, &fmt[0], args);
//    va_end(args);
//    printk(cBspMsgBuf);
//}




void _xygpu_osk_abort(void)
{
    /* make a simple fault by dereferencing a NULL pointer */
    *(int *)0 = 0;
}

void _xygpu_osk_break(void)
{
    _xygpu_osk_abort();
}
