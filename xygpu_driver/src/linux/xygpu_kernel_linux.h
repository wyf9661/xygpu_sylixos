/*
 * This confidential and proprietary software may be used only as
 * authorised by a licensing agreement from JCY Limited
 * (C) COPYRIGHT 2008-2010 JCY Limited
 * ALL RIGHTS RESERVED
 * The entire notice above must be reproduced on all authorised
 * copies and copies may only be made to the extent permitted
 * by a licensing agreement from JCY Limited.
 */

#ifndef __XYGPU_KERNEL_LINUX_H__
#define __XYGPU_KERNEL_LINUX_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <system/device/pci/pciDev.h>
#include "xygpu_osk.h"

struct xygpu_device *to_xygpu_device(PCI_DEV_HANDLE dev);
_xygpu_osk_errcode_t initialize_kernel_device(void);
void terminate_kernel_device(void);

#ifdef __cplusplus
}
#endif

/*********************************************************************************************************
  读取设备寄存器信息 read32((addr_t)((ULONG)(ctrl)->addr + reg)).
*********************************************************************************************************/
#define PCI_NONE_READ(ctrl, reg)    0

// U50 (sietium)
// [sietium@centos7arm64 hello]$ lspci -d 10ee:8018 -vmm -n
// Slot:   06:00.0
// Class:  0700
// Vendor: 10ee
// Device: 8018
// SVendor:        10ee
// SDevice:        0007
// ProgIf: 01
// 
// KCU1500 (sietium)
// ....
// 
// [sietium@centos7arm64 ~]$ lspci -d 16c3:abcd -vmm -n
// Slot:   03:00.0
// Class:  0300
// Vendor: 16c3
// Device: abcd
// Rev:    01

#define XYGPU_FPGA_PCI_VENDOR_ID   0x10EE
#define XYGPU_FPGA_PCI_DEVICE_ID   0x8018

#define XYGPU_FPGA_SUBSYS_VENDOR_ID   0x10EE
#define XYGPU_FPGA_SUBSYS_DEVICE_ID   0x0007

//class => 0x0700, class_mask: 0xffff00
//         PCI_CLASS_COMMUNICATION_SERIAL

#define XYGPU_FPGA2_PCI_VENDOR_ID   0x10EE
#define XYGPU_FPGA2_PCI_DEVICE_ID   0x8018

#define XYGPU_FPGA2_SUBSYS_VENDOR_ID   0x10EE
#define XYGPU_FPGA2_SUBSYS_DEVICE_ID   0x0007

//#define XYGPU_FPGA_PCI_VENDOR_ID   0x1111
//#define XYGPU_FPGA_PCI_DEVICE_ID   0x1112

#define XYGPU_PCI_VENDOR_ID       0x16C3
#define XYGPU_PCI_DEVICE_ID       0xabcd

// test

#define PCI_BAR_MAX_CNT 3
// bar0, bar1 (not use), bar2 (ddr)
#define IRQ_NUM 4

enum xygpu_asic_type {
        XYGPU_FPGA = 0,
        XYGPU_FPGA2,
        XYGPU_01,
        XYGPU_TYPE_MAX,
};

struct xygpu_pcie_info {
    PCI_DEV_HANDLE pdev;
        void __iomem *pci_mmio_bar[PCI_BAR_MAX_CNT];
    unsigned int len_bar[PCI_BAR_MAX_CNT];
    u64 start_bar[PCI_BAR_MAX_CNT];
    int irq_vector_num;
    int msi_enabled;
    int irqs[IRQ_NUM];
    struct xygpu_asic_dev_info *xygpu_dev_info;
    //struct gb_edma_para edma_para;
};

struct xygpu_asic_dev_info {
	enum xygpu_asic_type xygpu_type;
	unsigned long vram_size;
	int pci_bar_cnt;
	int gpu_reg_bar_id;
	int ddr_bar_id;
	unsigned long gpu_base;
	unsigned long gpu_base_offset;
	unsigned long ddr_base;
	unsigned long ddr_base_offset;
};

#define XYGPU_FPGA_DEV_INFO \
	.xygpu_type = XYGPU_FPGA, \
	.vram_size = 512 * 1024 * 1024, \
	.pci_bar_cnt  = 3, \
	.gpu_reg_bar_id  = 0, \
	.ddr_bar_id = 2, \
	.gpu_base = 0x58000000, \
	.gpu_base_offset  = 0x0, \
	.ddr_base = 0x1000000000, \
	.ddr_base_offset = 0x0, \

#define XYGPU_FPGA2_DEV_INFO \
	.xygpu_type = XYGPU_FPGA2, \
	.vram_size = 512 * 1024 * 1024, \
	.pci_bar_cnt  = 3, \
	.gpu_reg_bar_id  = 0, \
	.ddr_bar_id = 2, \
	.gpu_base = 0x58000000, \
	.gpu_base_offset  = 0x0, \
	.ddr_base = 0x1000000000, \
	.ddr_base_offset = 0x0, \

#define XYGPU_01_DEV_INFO \
	.xygpu_type = XYGPU_01, \
	.vram_size = 512 * 1024 * 1024, \
	.pci_bar_cnt  = 3, \
	.gpu_reg_bar_id  = 0, \
	.ddr_bar_id = 2, \
	.gpu_base = 0x0, \
	.gpu_base_offset  = 0x0, \
	.ddr_base = 0x0, \
	.ddr_base_offset = 0x0, \


struct xygpu_device {
    PCI_DEV_HANDLE pci_dev;
//    struct list_head entry;
//    struct device *dev;
	//struct miscdevice mdev;
	struct xygpu_pcie_info *pcie_info;
};

#define XYGPU_PCI_NAME "xygpu_pcie"

#endif /* __XYGPU_KERNEL_LINUX_H__ */
