/*
 * xygpu_platform.c
 *
 *  Created on: Oct 18, 2022
 *      Author: paky
 */
#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <module.h>
#include <linux/kernel.h>


#include "xygpu_kernel_common.h"
#include "xygpu_osk.h"
#include "xygpu_platform.h"


_xygpu_osk_errcode_t xygpu_platform_init(_xygpu_osk_resource_t *resource)
{
    XYGPU_SUCCESS;
}

_xygpu_osk_errcode_t xygpu_platform_deinit(_xygpu_osk_resource_type_t *type)
{
    XYGPU_SUCCESS;
}

_xygpu_osk_errcode_t xygpu_platform_powerdown(u32 cores)
{
    XYGPU_SUCCESS;
}

_xygpu_osk_errcode_t xygpu_platform_powerup(u32 cores)
{
    XYGPU_SUCCESS;
}
