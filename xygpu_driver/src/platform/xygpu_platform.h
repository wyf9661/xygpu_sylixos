/*
 * xygpu_platform.h
 *
 *  Created on: Oct 18, 2022
 *      Author: paky
 */

#ifndef SRC_PLATFORM_XYGPU_PLATFORM_H_
#define SRC_PLATFORM_XYGPU_PLATFORM_H_

/**
 * @file xygpu_platform.h
 * Platform specific GA driver functions
 */

#include "xygpu_osk.h"

#if !USING_XYGPU_PMM
/* @brief System power up/down cores that can be passed into xygpu_platform_powerdown/up() */
#define XYGPU_PLATFORM_SYSTEM  0
#endif

/** @brief Platform specific setup and initialisation of XYGPU
 *
 * This is called from the entrypoint of the driver to initialize the platform
 * When using PMM, it is also called from the PMM start up to initialise the
 * system PMU
 *
 * @param resource This is NULL when called on first driver start up, else it will
 * be a pointer to a PMU resource
 * @return _XYGPU_OSK_ERR_OK on success otherwise, a suitable _xygpu_osk_errcode_t error.
 */
_xygpu_osk_errcode_t xygpu_platform_init(_xygpu_osk_resource_t *resource);

/** @brief Platform specific deinitialisation of XYGPU
 *
 * This is called on the exit of the driver to terminate the platform
 * When using PMM, it is also called from the PMM termination code to clean up the
 * system PMU
 *
 * @param type This is NULL when called on driver exit, else it will
 * be a pointer to a PMU resource type (not the full resource)
 * @return _XYGPU_OSK_ERR_OK on success otherwise, a suitable _xygpu_osk_errcode_t error.
 */
_xygpu_osk_errcode_t xygpu_platform_deinit(_xygpu_osk_resource_type_t *type);

/** @brief Platform specific powerdown sequence of XYGPU
 *
 * Called as part of platform init if there is no PMM support, else the
 * PMM will call it.
 *
 * @param cores This is XYGPU_PLATFORM_SYSTEM when called without PMM, else it will
 * be a mask of cores to power down based on the xygpu_pmm_core_id enum
 * @return _XYGPU_OSK_ERR_OK on success otherwise, a suitable _xygpu_osk_errcode_t error.
 */
_xygpu_osk_errcode_t xygpu_platform_powerdown(u32 cores);

/** @brief Platform specific powerup sequence of XYGPU
 *
 * Called as part of platform deinit if there is no PMM support, else the
 * PMM will call it.
 *
 * @param cores This is XYGPU_PLATFORM_SYSTEM when called without PMM, else it will
 * be a mask of cores to power down based on the xygpu_pmm_core_id enum
 * @return _XYGPU_OSK_ERR_OK on success otherwise, a suitable _xygpu_osk_errcode_t error.
 */
_xygpu_osk_errcode_t xygpu_platform_powerup(u32 cores);


#endif /* SRC_PLATFORM_XYGPU_PLATFORM_H_ */
