/*
 * config.h
 *
 *  Created on: Oct 18, 2022
 *      Author: paky
 */

#ifndef SRC_ARCH_CONFIG_H_
#define SRC_ARCH_CONFIG_H_


/* Configuration for the EB platform with ZBT memory enabled */

static _xygpu_osk_resource_t arch_configuration [] =
{
    {
        .type = XYGPU400GP,
        .description = "GA-400 GP",
        //.base = 0xa0000000,
        .base = 0x58200000,
        //.irq = 87,
        .irq = 24,
        .irq_idx = 0,
        .mmu_id = 1
    },
    {
        .type = XYGPU400PP,
        //.base = 0xa0008000,
        .base = 0x58208000,
        //.irq = 88,
        .irq = 24,
        //.irq_idx = 1,
        .irq_idx = 0,
        .description = "GA-400 PP",
        .mmu_id = 2
    },
#if USING_MMU
    {
        .type = MMU,
        //.base = 0xa0003000,
        .base = 0x58203000,
        //.irq = 66,
        .irq = 26,
        .irq_idx = 2,
        .description = "GA-400 MMU for GP",
        .mmu_id = 1
    },
    {
        .type = MMU,
        //.base = 0xa0004000,
        .base = 0x58204000,
        //.irq = 63,
        .irq = 27,
        .irq_idx = 3,
        .description = "GA-400 MMU for PP",
        .mmu_id = 2
    },
#endif
#if ! ONLY_ZBT
    {
        .type = MEMORY,
        .description = "GA SDRAM remapped to baseboard",
        .cpu_usage_adjust = 0,
        //.cpu_usage_adjust = 0x1000000000,
        .alloc_order = 0, /* Highest preference for this memory */
#if XYGPU_USE_UNIFIED_MEMORY_PROVIDER != 0
        //.base = 0x2A000000, /* Reserving 32MB for UMP devicedriver */
        .base =       0x100A000000, /* Reserving 32MB for UMP devicedriver */
        //.base =   0xffff00016A000000,
        //.base = 0xA000000, /* Reserving 32MB for UMP devicedriver */
        .size = 0x16000000,
#else
        //.base = 0x28000000,
        .base =       0x1008000000,
        //.base =   0xffff000168000000,
        //.base = 0x8000000,
        .size = 0x18000000,
#endif /* XYGPU_USE_UNIFIED_MEMORY_PROVIDER != 0 */
        .flags = _XYGPU_CPU_WRITEABLE | _XYGPU_CPU_READABLE | _XYGPU_PP_READABLE | _XYGPU_PP_WRITEABLE |_XYGPU_GP_READABLE | _XYGPU_GP_WRITEABLE
    },
#endif
#if USING_ZBT
    {
        .type = MEMORY,
        .description = "GA ZBT",
        //.cpu_usage_adjust = 0x1000000000,
        .cpu_usage_adjust = 0,
        .alloc_order = 5, /* Medium preference for this memory */
        //.base = 0x20000000,
        .base = 0x1000000000,
        //.base = 0xffff000160000000,
        //.base = 0x0,
        .size = 0x01800000,
        .flags = _XYGPU_CPU_WRITEABLE | _XYGPU_CPU_READABLE | _XYGPU_PP_READABLE | _XYGPU_PP_WRITEABLE |_XYGPU_GP_READABLE | _XYGPU_GP_WRITEABLE
    },
#endif
    {
        .type = MEM_VALIDATION,
        .description = "Framebuffer",
        //.base = 0x20000000,
        //.cpu_usage_adjust = 0x1000000000,
        .cpu_usage_adjust = 0,
        .base = 0x1000000000,
        //.base = 0xffff000160000000,
        .size = 0x00800000,
        .flags = _XYGPU_CPU_WRITEABLE | _XYGPU_CPU_READABLE | _XYGPU_PP_WRITEABLE | _XYGPU_PP_READABLE
    },
    {
        .type = XYGPU400L2,
        //.base = 0xa0001000,
        .base = 0x58201000,
        .description = "GA-400 L2 cache"
    },
};

#endif /* SRC_ARCH_CONFIG_H_ */
